package com.open.fs.starter.constants;

/**
 * <p>Title: Open DTS</p>
 * <p>Description: Open DTS</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author rotten
 * @version 1.0
 * @date 2021/5/18 11:33
 */

public class MinioConstants {
    public static final String MINIO = "minio";
    public static final String MINIO_ENDPOINT = "minio.endpoint";
    public static final String MINIO_ACCESS_KEY = "minio.access-key";
    public static final String MINIO_SECRET_KEY = "minio.secret-key";
}
