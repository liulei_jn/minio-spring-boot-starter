package com.open.fs.starter.operation;

import io.minio.*;
import io.minio.errors.*;
import io.minio.http.Method;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

/**
 * @author zsj
 * @version 1.0.0
 * @description MinioTemplate
 * @date 2020/7/22
 */
@Slf4j
public class MinioTemplate implements MinIoOperation {
    @Autowired
    private MinioClient minioClient;

    @Override
    public String upload(InputStream stream, long objectSize, long partSize, String bucketName, String objectName, String contentType) {

        try {
            if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
                 minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            }
            ObjectWriteResponse objectWriteResponse = minioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .stream(stream, objectSize, partSize)
                    .contentType(contentType)
                    .build());
            return getObjectUrl(objectName, bucketName);
        } catch (ErrorResponseException | InsufficientDataException | InternalException  | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException | XmlParserException  e) {
            e.printStackTrace();
        }
        return StringUtils.EMPTY;
    }

    @Override
    public void download(String fileName, String objectName, String bucketName) {
        try {
            minioClient.downloadObject(DownloadObjectArgs.builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .filename(fileName)
                    .build());
        } catch (ErrorResponseException | InsufficientDataException | InternalException  | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException | XmlParserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getObjectUrl(String objectName, String bucketName, int dur, TimeUnit unit) {
        try {
            return minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                    .bucket(bucketName)
                    .method(Method.GET)
                    .object(objectName)
                    .expiry(dur, unit)
                    .build());
        } catch (ErrorResponseException | InsufficientDataException | InternalException  | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | XmlParserException | ServerException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getObjectUrl(String objectName, String bucketName) {
        try {
            return minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs
                    .builder().
                    bucket(bucketName)
                    .method(Method.GET)
                    .object(objectName).build());
        } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException | XmlParserException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void delete(String bucketName,String objectName) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .build());
        } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException | XmlParserException e) {
            e.printStackTrace();
        }
    }
}
