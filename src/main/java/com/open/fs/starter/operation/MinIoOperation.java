package com.open.fs.starter.operation;


import java.io.InputStream;
import java.util.concurrent.TimeUnit;


/**
 * @author zsj
 * @version 1.0.0
 * @description MinIoOperation
 * @date 2020/7/22
 */
public interface MinIoOperation {

    /**
     *
     * 存储对象上传
     * @param bucketName
     * @param objectName
     */
    String upload(InputStream stream,long objectSize, long partSize, String bucketName, String objectName,String contentType);

    /**
     * 存储对象下载
     * @param objectName
     * @param bucketName
     */
     void download(String fileName,String objectName, String bucketName);


    /**
     * 返回对象存储url
     * @param objectName
     * @param bucketName
     */
    String getObjectUrl(String objectName, String bucketName,
                                       int dur, TimeUnit timeUnit);

    /**
     * 返回对象存储url
     * @param objectName
     * @param bucketName
     */
    String getObjectUrl(String objectName, String bucketName);


    /**
     * 删除
     * @param bucketName
     * @return
     */
     void delete(String bucketName,String objectName);

}
