package com.open.fs.starter.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

/**
 * <p>Title: Open DTS</p>
 * <p>Description: Open DTS</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/5/18 13:50
 */

public interface FileService {
    /**
     *
     * 存储对象上传
     * @param bucketName
     */

    String upload(MultipartFile file,String bucketName,String contentType) throws IOException;

    /**
     * 存储对象下载
     * @param objectName
     * @param bucketName
     */
    void download(String fileName,String objectName, String bucketName);


    /**
     * 返回对象存储url
     * @param objectName
     * @param bucketName
     */
    String getObjectUrl(String objectName, String bucketName,
                        int dur, TimeUnit timeUnit);

    /**
     * 返回对象存储url
     * @param objectName
     * @param bucketName
     */
    String getObjectUrl(String objectName, String bucketName);


    /**
     * 删除
     * @param bucketName
     * @return
     */
    void delete(String bucketName,String objectName);
}
