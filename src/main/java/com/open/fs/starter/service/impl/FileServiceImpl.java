package com.open.fs.starter.service.impl;

import com.open.fs.starter.operation.MinIoOperation;
import com.open.fs.starter.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

/**
 * <p>Title: Open DTS</p>
 * <p>Description: Open DTS</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/5/18 13:53
 */

public class FileServiceImpl implements FileService {
    @Autowired
    private MinIoOperation minIoOperation;

    private final long MIN_SIZE = 5 *1024 * 1024;

    @Override
    public String upload(MultipartFile file, String bucketName , String contentType) throws IOException {
        long partSize = file.getSize();
        if(partSize < MIN_SIZE){
            partSize = MIN_SIZE;
        }
        return minIoOperation.upload(file.getInputStream(), file.getSize(), partSize, bucketName, file.getOriginalFilename(), contentType);
    }

    @Override
    public void download(String fileName, String objectName, String bucketName) {
        minIoOperation.download(fileName,objectName,bucketName);
    }

    @Override
    public String getObjectUrl(String objectName, String bucketName, int dur, TimeUnit timeUnit) {
        return minIoOperation.getObjectUrl(objectName,bucketName,dur,timeUnit);
    }

    @Override
    public String getObjectUrl(String objectName, String bucketName) {
        return minIoOperation.getObjectUrl(objectName,bucketName);
    }

    @Override
    public void delete(String bucketName, String objectName) {
            minIoOperation.delete(bucketName,objectName);
    }
}
