package com.open.fs.starter.configuration;

import com.nepxion.banner.BannerConstant;
import com.nepxion.banner.Description;
import com.nepxion.banner.LogoBanner;
import com.nepxion.banner.NepxionBanner;
import com.open.fs.starter.constants.MinioConstants;
import com.open.fs.starter.exception.MinioException;
import com.open.fs.starter.operation.MinIoOperation;
import com.open.fs.starter.operation.MinioTemplate;
import com.open.fs.starter.service.FileService;
import com.open.fs.starter.service.impl.FileServiceImpl;
import com.taobao.text.Color;
import io.minio.MinioClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * <p>Title: Open DTS</p>
 * <p>Description: Open DTS</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author rotten
 * @version 1.0
 * @date 2021/5/18 11:26
 */

@Configuration
public class MinioConfigAutoConfiguration {
    @Autowired
    private Environment environment;

    @Bean
    public MinioClient minioClient(){
        String endpoint = environment.getProperty(MinioConstants.MINIO_ENDPOINT);
        if (StringUtils.isEmpty(endpoint)) {
            throw new MinioException(MinioConstants.MINIO_ENDPOINT + " can't be null or empty");
        }

        String accessKey = environment.getProperty(MinioConstants.MINIO_ACCESS_KEY);
        if (StringUtils.isEmpty(accessKey)) {
            throw new MinioException(MinioConstants.MINIO_ACCESS_KEY + " can't be null or empty");
        }

        String secretKey = environment.getProperty(MinioConstants.MINIO_SECRET_KEY);
        if (StringUtils.isEmpty(secretKey)) {
            throw new MinioException(MinioConstants.MINIO_SECRET_KEY + " can't be null or empty");
        }

        MinioClient minioClient =
                MinioClient.builder()
                        .endpoint(endpoint)
                        .credentials(accessKey, secretKey)
                        .build();
        return minioClient;
    }

    @Bean
    public MinIoOperation minIoOperation(){
        return new MinioTemplate();
    }

    @Bean
    public FileService fileService(){
        return new FileServiceImpl();
    }

    static {
        LogoBanner logoBanner = new LogoBanner(MinioConfigAutoConfiguration.class, "/com/open/fs/starter/resource/logo.txt", "Welcome to Nepxion", 9, 5, new Color[] { Color.red, Color.green, Color.cyan, Color.blue, Color.yellow, Color.magenta, Color.red, Color.green, Color.cyan }, true);
        NepxionBanner.show(logoBanner, new Description("Config:", MinioConstants.MINIO, 0, 1), new Description(BannerConstant.GITHUB + ":", BannerConstant.NEPXION_GITHUB + "/Discovery", 0, 1));
    }

}
