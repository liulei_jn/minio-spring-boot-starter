package com.open.fs.starter.exception;

/**
 * <p>Title: Open DTS</p>
 * <p>Description: Open DTS</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author rotten
 * @version 1.0
 * @date 2021/5/18 11:26
 */

public class MinioException extends RuntimeException {
    private static final long serialVersionUID = 7975167663357170655L;

    public MinioException() {
        super();
    }

    public MinioException(String message) {
        super(message);
    }

    public MinioException(String message, Throwable cause) {
        super(message, cause);
    }

    public MinioException(Throwable cause) {
        super(cause);
    }
}