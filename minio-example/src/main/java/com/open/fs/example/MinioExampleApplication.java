package com.open.fs.example;

import com.open.fs.starter.operation.MinIoOperation;
import com.open.fs.starter.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@SpringBootApplication
public class MinioExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinioExampleApplication.class, args);
    }

    @Autowired
    private FileService fileService;

    @PostMapping(value = "upload/{bucket}")
    public String  upload(@PathVariable String bucket, MultipartFile file) throws IOException {
        return fileService.upload(file,bucket,"application/octet-stream");
    }
}
